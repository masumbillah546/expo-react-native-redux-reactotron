import { Text, StyleSheet, View, Button } from "react-native";
import { useSelector, useDispatch } from "react-redux";
// import { increment, decrement } from "../redux/reducers/counter";
import { doIncrement, doDecrement } from "../redux/actions";

export default function Home() {
  const dispatch = useDispatch();
  const { count1, count2 } = useSelector((state) => ({
    count1: state.counter.count,
    count2: state.counter2.count,
  }));

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Redux Template</Text>
      <Text style={styles.subtitle}>Counter(increment by 3) value: {count1}</Text>
      <Text style={styles.subtitle}>Counter2(decrement by 2) value: {count2}</Text>
      <Button
        title="Increment"
        onPress={() => {
          dispatch(doIncrement(3));
        }}
      />
      <View style={{ marginTop: 10 }}>
       <Button
        title="decrement"
        onPress={() => {
          dispatch(doDecrement(2));
        }}
      />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontSize: 32,
    fontWeight: "bold",
    marginBottom: 30,
  },
  subtitle: {
    fontSize: 18,
    fontWeight: "500",
    marginBottom: 15,
  },
});
