import { increment, decrement } from "../reducers/counter";

export const doIncrement = (payload) => {
  return dispatch => {
    dispatch(increment(payload))
  }
}

export const doDecrement = (payload) => {
  return dispatch => {
    dispatch(decrement(payload))
  }
}