import { configureStore } from "@reduxjs/toolkit";
import counterReducer, {rootReducer} from "../reducers/counter";
import Reactotron from './ReactotronConfig'

if(__DEV__) {
  import('./ReactotronConfig').then(() => console.log('Reactotron Configured'))
}

export const store = configureStore({
  // reducer: {
  //   counter: counterReducer,
  // },
  reducer: rootReducer,
  enhancers: [Reactotron.createEnhancer()]
});
