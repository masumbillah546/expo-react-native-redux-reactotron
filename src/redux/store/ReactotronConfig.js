import Reactotron from 'reactotron-react-native'
import { reactotronRedux } from 'reactotron-redux'
// import AsyncStorage from '@react-native-async-storage/async-storage';


// then add it to the plugin list
const reactotron = Reactotron
  // .setAsyncStorageHandler(AsyncStorage) // AsyncStorage would either come from `react-native` or `@react-native-community/async-storage` depending on where you get it from
  .configure({ name: 'React Native Demo' })
  .use(reactotronRedux()) //  <- here i am!
  .connect() //Don't forget about me!
  
export default reactotron