import { combineReducers } from 'redux';
import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  count: 0,
};

const counterSlice = createSlice({
  name: "counter",
  initialState,
  reducers: {
    increment: (state, { payload }) => ({
    ...state,
     count: state.count + payload,
    }),
  },
});

const counterSlice2 = createSlice({
  name: "counter2",
  initialState,
  reducers: {
    decrement: (state, { payload }) => ({
      ...state,
       count: state.count - payload,
    }),
  },
});

export const { increment } = counterSlice.actions;
export const { decrement } = counterSlice2.actions;

export const rootReducer = combineReducers({
  counter: counterSlice.reducer,
  counter2: counterSlice2.reducer,
});

export default counterSlice.reducer;
